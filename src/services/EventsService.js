import axios from "axios";

export class EventsService {
    constructor() {
        this.url = 'http://localhost:8000/api-fencing/events'
    }
    async getAll(){
        let events = await axios.get(this.url);
        return events.data;
    }
}